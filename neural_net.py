from keras.models import Sequential
from keras.layers import Dense
from keras.utils.np_utils import to_categorical
from keras.optimizers import Adam
import numpy as np

model = Sequential()

##########################################################
################### Create neural net ####################
##########################################################
# Layers
model.add(Dense(8, activation='relu', input_dim=4))
model.add(Dense(16, activation='relu'))
model.add(Dense(32, activation='relu'))
model.add(Dense(16, activation='relu'))
model.add(Dense(8, activation='relu'))
model.add(Dense(3, activation='softmax'))
# softmax determines the case when chances for all 3 categories are equal


# Classify outputs
# output 0 = Iris-setosa
# output 1 = Iris-versicolor
# output 2 = Iris-virginica

# learning rate, the default is 0.001
# too small will learn nothing, too large will keep jumping over and never learn
opt = Adam(lr=0.005)
model.compile(
    optimizer=opt,
    loss='categorical_crossentropy',
    metrics=['accuracy']
)


# Import data from csv, similar to panda
data = np.genfromtxt('iris.csv', delimiter=',')
x_train = data[1:, :4]
y_train = to_categorical(data[1:, 4])

# Shuffle data for validation dataset
perm = np.random.permutation(y_train.shape[0])
x_train = x_train[perm]
y_train = y_train[perm]


##########################################################
####################### Feed data ########################
##########################################################
# Take large learning steps with large learning rate
model.fit(
    x_train,
    y_train,
    epochs=100,
    validation_split=0.2
)

##########################################################
###################### Second feed #######################
##########################################################
# Fine-grain model with smaller learning rate that takes smaller steps
model.optomizer = Adam(lr=0.0001)

model.fit(
    x_train,
    y_train,
    epochs=100,
    validation_split=0.2
)

##########################################################
###################### Third feed #######################
##########################################################
# Continue to fine-grain model
model.optomizer = Adam(lr=0.0001)

model.fit(
    x_train,
    y_train,
    epochs=100,
    validation_split=0.2
)


model.save('iris.h5')
