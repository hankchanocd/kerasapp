# KerasApp

> Practice with Keras

- Train a neural net
- Configure the layers' width and depth
- Save a neural net
- Predict on a dataset with the saved neural net
- Classify the outputs of neural net
- Multi-classify the outputs of neural net
- Adjust learning rates with Adam
- Continue training with pretrained neural net
